# Thanos General Troubleshooting

Thanos docs: https://thanos.io/operating/troubleshooting.md/

## Alert: ThanosGrpcErrorRate

* Check target logs.
* Check status of remote client/server.
